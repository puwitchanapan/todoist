*** Settings ***
Library     SeleniumLibrary


*** Variables ***
${Browser}    chrome
${Server}     https://todoist.com/users/showLogin   

*** Keywords ***
Chrome Open Browser
  
    Open Browser    ${Server}    ${Browser}
    Maximize Browser Window
    Set Selenium Speed    0.25

Login 
    Input Text    //*[@id="email"]    puwitchanapan@gmail.com
    Input Text    //*[@id="password"]    1234
    Submit Form
    Wait Until Page Contains    Wrong email or password.
    sleep    2
    #Reload Page
    #Input Text    //*[@id="user"]    puwitchanapan@gmail.com
    Input Text    //*[@id="password"]    testtodoist
    Submit Form
    sleep    3

Add Task
    Click Element    //*[@id="agenda_view"]/div/ul/li[4]/a
    Input Text       //*[@id="editor"]/div/div[1]/ul/li/div/div[1]/ul/li[3]/div/form/div[1]/div[1]/div/div/div/div/div/div    test
    Submit Form        
    

Update Task
    Click Element    //*[@id="item_3817317025"]/div[2]/div[2]/div/div[1] 
    click Element    //*[@id="tabs-9-panel-subtasks"]/div/div/button
    Input Text    //*[@id="tabs-9-panel-subtasks"]/div/ul[1]/li/form/div[1]/div[1]/div/div[2]/div/div/div/div   testtest
    Submit Form
                  
Delect TASK
    Click Element    //*[@id="item_3817317025"]/div[2]/div[2]/div/div[1]
    Click Element    //*[@id="reactist-modal-box-18"]/section/div/div[2]/div/div[3]/div/button[5]
    Click Element    /html/body/div[14]/div/ul/li[5]
    Click Element    //*[@id="reactist-modal-box-25"]/footer/button[2]

# Sort task list    

    # Click Element    //*[@id="item_3817144790"]/div[2]/div[2]/div/div[1]
    # Click Element    //*[@id="tabs-9-panel-subtasks"]/div/div/button
    # Input Text    //*[@id="tabs-9-panel-subtasks"]/div/ul[1]/li/form/div[1]/div[1]/div/div/div/div/div/div    test todoist
    # Click Element    //*[@id="tabs-9-panel-subtasks"]/div/ul[1]/li/form/div[2]/button[1]
*** Test Cases ***
#     Open Website
#     Login Negative

Test
    Chrome Open Browser
    Login
    Add TASK
    Update TASK
    Delect TASK
    # Sort task list


